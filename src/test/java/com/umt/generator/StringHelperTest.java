package com.umt.generator;

import com.umt.generator.base.SpringTestSupport;
import com.umt.generator.util.StringHelper;
import org.junit.Test;

public class StringHelperTest extends SpringTestSupport {

    @Test
    public void test() {
        String modelName = StringHelper.underLine2Hump("MODEL_NAME");
        String modelType = StringHelper.underLine2Hump("model_type");
        System.err.println(modelName);
        System.err.println(modelType);
    }
}
