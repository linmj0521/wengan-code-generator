package com.umt.generator.dao;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TableInfo {

    private String name;

    private String entityName;

    private String type;

    private String remark;

    private List<ColumnInfo> columns;

    private boolean existDate = false;

    private boolean existBigDecimal = false;

    private Long entityUID;

    private Long specUID;
}
