package com.umt.generator;

import com.google.common.collect.Maps;
import com.umt.generator.constants.*;
import com.umt.generator.dao.TableInfo;
import com.umt.generator.util.DbInfoUtil;
import com.umt.generator.util.FreeMarkerTemplateUtil;
import freemarker.template.Template;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "table")
public class CodeGenerate {

    @Setter
    private Boolean singleTable;

    // 表名称集合
    @Setter
    @Getter
    private Map<String, String> tableNames;
    
    @Getter
    @Setter
    private String tableName;
    
    @Getter
    @Setter
    private String entityName;

    @Autowired
    private BaseClassInfo baseClassInfo;

    @Value("disk-path")
    private String diskPath;

    @Autowired
    private PackageInfo packageInfo;
    
    @Autowired
    private PackageInfoSingle packageInfoSingle;

    // 表信息集合
    private List<TableInfo> tableInfoList;

    private Map<String,Object> dataMap = Maps.newHashMapWithExpectedSize(8);

    public void generate() throws Exception {
        if (BooleanUtils.isTrue(singleTable)) {
            generateSingleTable();
        } else {
            generateMultiTable();
        }
    }

    public void generateMultiTable() throws Exception {
        dataMap.put("author", AuthorInfo.AUTHOR);
        dataMap.put("createDate", AuthorInfo.CREATE_DATE);
        dataMap.put("baseClassInfo", baseClassInfo);

        tableInfoList = DbInfoUtil.getTableInfo(tableNames);
        if (CollectionUtils.isNotEmpty(tableInfoList)) {
            for (TableInfo tableInfo : tableInfoList) {
                tableInfo.setEntityUID(RandomUtils.nextLong());
                tableInfo.setSpecUID(RandomUtils.nextLong());
                dataMap.put("tableInfo", tableInfo);
                dataMap.put("entityName", tableInfo.getEntityName());

                if (StringUtils.isNotBlank(TemplateInfo.entity)) {
                    dataMap.put("entityPackage", packageInfo.getEntityPackage().get(tableInfo.getName()));
                    generateEntity();
                }

                if (StringUtils.isNotBlank(TemplateInfo.spec)) {
                    dataMap.put("specPackage", packageInfo.getSpecPackage().get(tableInfo.getName()));
                    generateSpec();
                }

                if (StringUtils.isNotBlank(TemplateInfo.mapperInterface)) {
                    dataMap.put("mapperInterfacePackage", packageInfo.getMapperInterfacePackage().get(tableInfo.getName()));
                    generateMapperInterface();
                }

                if (StringUtils.isNotBlank(TemplateInfo.mapper)) {
                    dataMap.put("mapperPath", packageInfo.getMapperPath().get(tableInfo.getName()));
                    generateMapper();
                }

                if (StringUtils.isNotBlank(TemplateInfo.serviceInterface)) {
                    dataMap.put("serviceInterfacePackage", packageInfo.getServiceInterfacePackage().get(tableInfo.getName()));
                    generateServiceInterface();
                }

                if (StringUtils.isNotBlank(TemplateInfo.serviceImpl)) {
                    dataMap.put("serviceImplPackage", packageInfo.getServiceImplPackage().get(tableInfo.getName()));
                    dataMap.put("serviceProviderVersion", packageInfo.getServiceProviderVersion().get(tableInfo.getName()));
                    generateServiceImpl();
                }

                if (StringUtils.isNotBlank(TemplateInfo.controller)) {
                    dataMap.put("controllerPackage", packageInfo.getControllerPackage().get(tableInfo.getName()));
                    dataMap.put("controllerRegistry", packageInfo.getControllerRegistry().get(tableInfo.getName()));
                    dataMap.put("viewPath", packageInfo.getViewPath().get(tableInfo.getName()));
                    dataMap.put("jsPath", packageInfo.getJsPath().get(tableInfo.getName()));
                    generateView();
                }
            }
        }
    }
    
    public void generateSingleTable() throws Exception {
        dataMap.put("author", AuthorInfo.AUTHOR);
        dataMap.put("createDate", AuthorInfo.CREATE_DATE);
        dataMap.put("baseClassInfo", baseClassInfo);
        TableInfo tableInfo = DbInfoUtil.getTableInfo(tableName, entityName);
        if (null != tableInfo) {
            tableInfo.setEntityUID(RandomUtils.nextLong());
            tableInfo.setSpecUID(RandomUtils.nextLong());
            dataMap.put("tableInfo", tableInfo);
            dataMap.put("entityName", tableInfo.getEntityName());

            if (StringUtils.isNotBlank(TemplateInfo.entity)) {
                dataMap.put("entityPackage", packageInfoSingle.getEntityPackage());
                generateEntity();
            }

            if (StringUtils.isNotBlank(TemplateInfo.spec)) {
                dataMap.put("specPackage", packageInfoSingle.getSpecPackage());
                generateSpec();
            }

            if (StringUtils.isNotBlank(TemplateInfo.mapperInterface)) {
                dataMap.put("mapperInterfacePackage", packageInfoSingle.getMapperInterfacePackage());
                generateMapperInterface();
            }

            if (StringUtils.isNotBlank(TemplateInfo.mapper)) {
                dataMap.put("mapperPath", packageInfoSingle.getMapperPath());
                generateMapper();
            }

            if (StringUtils.isNotBlank(TemplateInfo.serviceInterface)) {
                dataMap.put("serviceInterfacePackage", packageInfoSingle.getServiceInterfacePackage());
                generateServiceInterface();
            }

            if (StringUtils.isNotBlank(TemplateInfo.serviceImpl)) {
                dataMap.put("serviceImplPackage", packageInfoSingle.getServiceImplPackage());
                dataMap.put("serviceProviderVersion", packageInfoSingle.getServiceProviderVersion());
                generateServiceImpl();
            }

            if (StringUtils.isNotBlank(TemplateInfo.controller)) {
                dataMap.put("controllerPackage", packageInfoSingle.getControllerPackage());
                dataMap.put("controllerRegistry", packageInfoSingle.getControllerRegistry());
                dataMap.put("viewPath", packageInfoSingle.getViewPath());
                dataMap.put("jsPath", packageInfoSingle.getJsPath());
                dataMap.put("jsAssetPath", packageInfoSingle.getJsPath().substring(packageInfoSingle.getJsPath().indexOf(".") + 1));
                generateView();
            }
        }
    }

    private void generateEntity() throws Exception {
        String path = TemplateInfo.entityPath + ((String) dataMap.get("entityPackage")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File dirs = new File(path);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
        path += dataMap.get("entityName") + ".java";
        String templateName = TemplateInfo.entity;
        File mapperFile = new File(path);
        generateFileByTemplate(templateName, mapperFile);
    }

    private void generateSpec() throws Exception {
        String path = TemplateInfo.specPath + ((String) dataMap.get("specPackage")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File dirs = new File(path);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
        path += dataMap.get("entityName") + "Spec.java";
        String templateName = TemplateInfo.spec;
        File mapperFile = new File(path);
        generateFileByTemplate(templateName, mapperFile);
    }

    private void generateMapperInterface() throws Exception {
        String path = TemplateInfo.mapperInterfacePath + ((String) dataMap.get("mapperInterfacePackage")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File dirs = new File(path);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
        path += dataMap.get("entityName") + "Mapper.java";
        String templateName = TemplateInfo.mapperInterface;
        File mapperFile = new File(path);
        generateFileByTemplate(templateName, mapperFile);
    }

    private void generateMapper() throws Exception {
        String path = TemplateInfo.mapperRootPath + ((String) dataMap.get("mapperPath")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File dirs = new File(path);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
        path += dataMap.get("entityName") + "Mapper.xml";
        String templateName = TemplateInfo.mapper;
        File mapperFile = new File(path);
        generateFileByTemplate(templateName, mapperFile);
    }

    private void generateServiceInterface() throws Exception {
        String path = TemplateInfo.serviceInterfacePath + ((String) dataMap.get("serviceInterfacePackage")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File dirs = new File(path);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
        path += "I" + dataMap.get("entityName") + "Service.java";
        String templateName = TemplateInfo.serviceInterface;
        File mapperFile = new File(path);
        generateFileByTemplate(templateName, mapperFile);
    }

    private void generateServiceImpl() throws Exception {
        String path = TemplateInfo.serviceImplPath + ((String) dataMap.get("serviceImplPackage")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File dirs = new File(path);
        if (!dirs.exists()) {
            dirs.mkdirs();
        }
        path += dataMap.get("entityName") + "ServiceImpl.java";
        String templateName = TemplateInfo.serviceImpl;
        File mapperFile = new File(path);
        generateFileByTemplate(templateName, mapperFile);
    }

    private void generateView() throws Exception {
        String controllerPath = TemplateInfo.controllerPath + ((String) dataMap.get("controllerPackage")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File controllerDirs = new File(controllerPath);
        if (!controllerDirs.exists()) {
            controllerDirs.mkdirs();
        }
        controllerPath += dataMap.get("entityName") + "Controller.java";
        String controllerTemplateName = TemplateInfo.controller;
        File controllerMapperFile = new File(controllerPath);
        generateFileByTemplate(controllerTemplateName, controllerMapperFile);


        String entityName = (String) dataMap.get("entityName");
        String entityNameFirstLowerCase = (new StringBuilder()).append(Character.toLowerCase(entityName.charAt(0))).append(entityName.substring(1)).toString();

        String jspPath = TemplateInfo.webappPath + TemplateInfo.viewRootPath + ((String) dataMap.get("viewPath")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File jspDirs = new File(jspPath);
        if (!jspDirs.exists()) {
            jspDirs.mkdirs();
        }
        jspPath += entityNameFirstLowerCase + ".jsp";
        String jspTemplateName = TemplateInfo.view;
        File jspMapperFile = new File(jspPath);
        generateFileByTemplate(jspTemplateName, jspMapperFile);


        String jsPath = TemplateInfo.webappPath + ((String) dataMap.get("jsPath")).replace(".", Constants.BACKSLASH) + Constants.BACKSLASH;
        File jsDirs = new File(jsPath);
        if (!jsDirs.exists()) {
            jsDirs.mkdirs();
        }
        jsPath += entityNameFirstLowerCase + ".js";
        String jsTemplateName = TemplateInfo.js;
        File jsMapperFile = new File(jsPath);
        generateFileByTemplate(jsTemplateName, jsMapperFile);
    }

    private void generateFileByTemplate(final String templateName, File file) throws Exception{
        Template template = FreeMarkerTemplateUtil.getTemplate(templateName);
        FileOutputStream fos = new FileOutputStream(file);

        Writer out = new BufferedWriter(new OutputStreamWriter(fos, "utf-8"),10240);
        template.process(dataMap, out);
    }
}
