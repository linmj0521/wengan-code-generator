package com.umt.generator;

import com.umt.generator.base.SpringTestSupport;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DbInfoUtilTest extends SpringTestSupport {

    @Autowired
    private CodeGenerate codeGenerate;

    @Test
    public void generate() throws Exception{
        codeGenerate.generate();
    }
}
