package com.umt.generator.constants;

/**
 * 常量类
 */
public class Constants {

    public static String EMPTY_STR = "";

    public static String PERCENT = "%";

    /**
     * 数据库
     */
    public static String DB_MYSQL = "mysql";
    public static String DB_ORACLE = "oracle";
    public static String DB_SQLSERVER = "sqlserver";

    /**
     * number类型
     */
    public static String DBTYPE_NUMBER = "NUMBER";

    /**
     * 文件后缀
     */
    public static String FILE_SUFFIX_JAVA = ".java";
    public static String FILE_SUFFIX_XML = ".xml";

    /**
     * 下划线
     */
    public static String CHARACTER_BOTTOM_LINE = "_";

    /**
     * 点号
     */
    public static String CHARACTER_POINT = ".";

    /**
     * 分号
     */
    public static String CHARACTER_SPLIT = ";";

    /**
     * 逗号
     */
    public static String CHARACTER_COMMA = ",";

    /**
     * 左括号
     */
    public static String CHARACTER_LEFT_PARENTHESES = "(";

    /**
     * 右括号
     */
    public static String CHARACTER_RIGHT_PARENTHESES = ")";

    public static String BACKSLASH = "\\";

    public static String SLASH = "/";
}
