package com.umt.generator.dao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ColumnInfo {

    private String name;

    private String columnName;

    private String type;

    private String remark;

    private int length;

    private int precision;
}
