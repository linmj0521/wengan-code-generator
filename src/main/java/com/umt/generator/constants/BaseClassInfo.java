package com.umt.generator.constants;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "base-class-info")
@Getter
public class BaseClassInfo {

    private String baseMapperReference;

    private String baseMapperName;

    private String baseEntityReference;

    private String baseEntityName;

    private String baseServiceReference;

    private String baseServiceName;

    private String baseServiceImplReference;

    private String baseServiceImplName;

    private String providerVersionReference;

    private String providerVersionName;

    public void setBaseMapperReference(String baseMapperReference) {
        if (StringUtils.isNotBlank(baseMapperReference)) {
            this.baseMapperName = baseMapperReference.substring(baseMapperReference.lastIndexOf(".") + 1);
        }
        this.baseMapperReference = baseMapperReference;
    }

    public void setBaseEntityReference(String baseEntityReference) {
        if (StringUtils.isNotBlank(baseEntityReference)) {
            this.baseEntityName = baseEntityReference.substring(baseEntityReference.lastIndexOf(".") + 1);
        }
        this.baseEntityReference = baseEntityReference;
    }

    public void setBaseServiceReference(String baseServiceReference) {
        if (StringUtils.isNotBlank(baseServiceReference)) {
            this.baseServiceName = baseServiceReference.substring(baseServiceReference.lastIndexOf(".") + 1);
        }
        this.baseServiceReference = baseServiceReference;
    }

    public void setBaseServiceImplReference(String baseServiceImplReference) {
        if (StringUtils.isNotBlank(baseServiceImplReference)) {
            this.baseServiceImplName = baseServiceImplReference.substring(baseServiceImplReference.lastIndexOf(".") + 1);
        }
        this.baseServiceImplReference = baseServiceImplReference;
    }

    public void setProviderVersionReference(String providerVersionReference) {
        if (StringUtils.isNotBlank(providerVersionReference)) {
            this.providerVersionName = providerVersionReference.substring(providerVersionReference.lastIndexOf(".") + 1);
        }
        this.providerVersionReference = providerVersionReference;
    }
}
