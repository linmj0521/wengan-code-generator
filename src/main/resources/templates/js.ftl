var ${tableInfo.entityName!} = window.${tableInfo.entityName!} = ${tableInfo.entityName!} || {};

$(function () {
    ${tableInfo.entityName!}.init();
});

// 初始化
${tableInfo.entityName!}.init = function () {

}