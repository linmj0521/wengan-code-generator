package com.umt.generator.constants;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "package-info")
@Getter
@Setter
public class PackageInfo {

    private Map<String, String> entityPackage;

    private Map<String, String> specPackage;

    private Map<String, String> mapperPath;

    private Map<String, String> mapperInterfacePackage;

    private Map<String, String> serviceInterfacePackage;

    private Map<String, String> serviceImplPackage;

    private Map<String, String> serviceProviderVersion;

    private Map<String, String> controllerPackage;

    private Map<String, String> controllerRegistry;

    private Map<String, String> viewPath;

    private Map<String, String> jsPath;

}
