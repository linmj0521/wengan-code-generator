package com.umt.generator.constants;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@ConfigurationProperties(prefix = "author-info")
public class AuthorInfo {

    // 创建人
    public static String AUTHOR;

    // 创建时间
    public static String CREATE_DATE;

    public void setAuthor(String author) {
        AuthorInfo.AUTHOR = author;
    }

    public void setCreateDate(String createDate) {
        if (StringUtils.isNotBlank(createDate)) {
            AuthorInfo.CREATE_DATE = createDate;
            return;
        }
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        AuthorInfo.CREATE_DATE = format.format(date);
    }
}
