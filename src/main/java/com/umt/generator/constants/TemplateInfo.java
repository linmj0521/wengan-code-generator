package com.umt.generator.constants;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "template-info")
@Getter
public class TemplateInfo {

    public static String basePath;

    public static String entity;

    public static String entityPath;

    public static String spec;

    public static String specPath;

    public static String mapper;

    public static String mapperRootPath;

    public static String mapperInterface;

    public static String mapperInterfacePath;

    public static String serviceInterface;

    public static String serviceInterfacePath;

    public static String serviceImpl;

    public static String serviceImplPath;

    public static String controller;

    public static String controllerPath;

    public static String view;

    public static String js;

    public static String webappPath;

    public static String viewRootPath;

    public void setBasePath(String basePath) {
        TemplateInfo.basePath = basePath;
    }

    public void setEntity(String entity) {
        TemplateInfo.entity = entity;
    }

    public void setEntityPath(String entityPath) {
        TemplateInfo.entityPath = entityPath;
    }

    public void setSpec(String spec) {
        TemplateInfo.spec = spec;
    }

    public void setSpecPath(String specPath) {
        TemplateInfo.specPath = specPath;
    }

    public void setMapper(String mapper) {
        TemplateInfo.mapper = mapper;
    }

    public void setMapperRootPath(String mapperRootPath) {
        TemplateInfo.mapperRootPath = mapperRootPath;
    }

    public void setMapperInterface(String mapperInterface) {
        TemplateInfo.mapperInterface = mapperInterface;
    }

    public void setMapperInterfacePath(String mapperInterfacePath) {
        TemplateInfo.mapperInterfacePath = mapperInterfacePath;
    }

    public void setServiceInterface(String serviceInterface) {
        TemplateInfo.serviceInterface = serviceInterface;
    }

    public void setServiceInterfacePath(String serviceInterfacePath) {
        TemplateInfo.serviceInterfacePath = serviceInterfacePath;
    }

    public void setServiceImpl(String serviceImpl) {
        TemplateInfo.serviceImpl = serviceImpl;
    }

    public void setServiceImplPath(String serviceImplPath) {
        TemplateInfo.serviceImplPath = serviceImplPath;
    }

    public void setController(String controller) {
        TemplateInfo.controller = controller;
    }

    public void setControllerPath(String controllerPath) {
        TemplateInfo.controllerPath = controllerPath;
    }

    public void setView(String view) {
        TemplateInfo.view = view;
    }

    public void setJs(String js) {
        TemplateInfo.js = js;
    }

    public void setWebappPath(String webappPath) {
        TemplateInfo.webappPath = webappPath;
    }

    public void setViewRootPath(String viewRootPath) {
        TemplateInfo.viewRootPath = viewRootPath;
    }
}
