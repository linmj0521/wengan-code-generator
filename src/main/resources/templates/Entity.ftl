package ${entityPackage!};

import ${baseClassInfo.baseEntityReference!};
<#if tableInfo.existBigDecimal>
import java.math.BigDecimal;
</#if>
<#if tableInfo.existDate>
import java.util.Date;
</#if>
import lombok.Data;

/**
 * Entity: ${tableInfo.remark!}
 * Table: ${tableInfo.name!}
 *
 * @author ${author!}
 * @date ${createDate!}
 */
@Data
public class ${tableInfo.entityName!} extends ${baseClassInfo.baseEntityName!}{

    private static final long serialVersionUID = ${tableInfo.entityUID!?c}L;
    <#if tableInfo.columns?exists>
        <#list tableInfo.columns as column>

    /**
     * ${column.remark!}
     */
    private ${column.type!} ${column.name!};
        </#list>
    </#if>
}