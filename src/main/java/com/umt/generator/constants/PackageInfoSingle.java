package com.umt.generator.constants;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "package-info-single")
@Getter
@Setter
public class PackageInfoSingle {

    private String entityPackage;

    private String specPackage;

    private String mapperPath;

    private String mapperInterfacePackage;

    private String serviceInterfacePackage;

    private String serviceImplPackage;

    private String serviceProviderVersion;

    private String controllerPackage;

    private String controllerRegistry;

    private String viewPath;

    private String jsPath;

}
