package ${mapperInterfacePackage!};

import ${baseClassInfo.baseMapperReference!};
import ${entityPackage!}.${tableInfo.entityName!};
import ${specPackage!}.${tableInfo.entityName!}Spec;

/**
 * Mapper: ${tableInfo.remark!}
 *
 * @author ${author!}
 * @date ${createDate!}
 */
public interface ${tableInfo.entityName!}Mapper extends ${baseClassInfo.baseMapperName!}<${tableInfo.entityName!}, ${tableInfo.entityName!}Spec> {

}