package com.umt.generator.util;

import com.umt.generator.constants.Constants;
import org.apache.commons.lang3.StringUtils;

public class StringHelper {

    public static String underLine2Hump(String underLine) {
        if (StringUtils.isNoneBlank(underLine)) {
            StringBuffer hump = new StringBuffer();
            String[] split = underLine.toLowerCase().split(Constants.CHARACTER_BOTTOM_LINE);
            for (String str : split) {
                if (hump.length() == 0) {
                    hump.append(str);
                } else {
                    hump.append(StringUtils.capitalize(str));
                }
            }
            return hump.toString();
        }
        return underLine;
    }
}
