package ${serviceInterfacePackage!};

import ${baseClassInfo.baseServiceReference!};
import ${entityPackage!}.${tableInfo.entityName!};
import ${specPackage!}.${tableInfo.entityName!}Spec;

/**
 * Service: ${tableInfo.remark!} 接口
 *
 * @author ${author!}
 * @date ${createDate!}
 */
public interface I${tableInfo.entityName!}Service extends ${baseClassInfo.baseServiceName!}<${tableInfo.entityName!}, ${tableInfo.entityName!}Spec> {

}