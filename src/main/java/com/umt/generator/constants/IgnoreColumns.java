package com.umt.generator.constants;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "ignore-columns")
public class IgnoreColumns {

    public static List<String> columnNames;

    public void setColumnNames(List<String> columnNames) {
        IgnoreColumns.columnNames = columnNames;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }
}
