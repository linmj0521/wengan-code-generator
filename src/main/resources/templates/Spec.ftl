package ${specPackage!};

import lombok.Data;
import java.io.Serializable;

/**
 * Spec: ${tableInfo.remark!}
 *
 * @author ${author!}
 * @date ${createDate!}
 */
@Data
public class ${tableInfo.entityName!}Spec implements Serializable {

    private static final long serialVersionUID = ${tableInfo.specUID!?c}L;

}