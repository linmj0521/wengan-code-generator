# wengan-code-generator
## 代码生成插件
### 配置文件：application-dev.yml

通过读取指定已有表信息，创建相关的  
Entity、Mapper、Dao接口、Service接口、Service实现、Controller、jsp、js  
仅支持Oracle、SQLServer、MySql数据库

入口：DbInfoUtilTest.generate();

配置一：  
db: 数据库连接配置（Oracle）  
  driver: oracle.jdbc.driver.OracleDriver  
  url: jdbc:oracle:thin:@//host:port/dbName  
  username: username  
  password: password  
  dbType: Oracle  
  dbName: dbName  
  
db: 数据库连接配置（sqlServer）  
  driver: net.sourceforge.jtds.jdbc.Driver  
  url: jdbc:jtds:sqlserver://host:port/dbName;useLOBs=false  
  username: username  
  password: password  
  dbType: sqlServer  
  dbName: dbName  
  
db: 数据库连接配置（mysql）  
  driver: com.mysql.cj.jdbc.Driver  
  url: jdbc:mysql://host:port/dbName?useUnicode=yes&characterEncoding=UTF8&allowMultiQueries=true  
  username: username  
  password: password  
  dbType: mySql  
  dbName: dbName  
    
配置二：  
table:  
  tableNames: #Map  
    key: value  
    key 需要生成的表名：value 生成实体类名
    
配置三：  
author-info:  
  author: 创建文件作者  
  createDate: 创建时间

配置四：  
ignore-columns:  
  columnNames: 数据库忽略字段（BaseEntity包含字段)
  
配置五：  
package-info: 包信息（key 需要生成的表名： value 对应包信息）  
  entityPackage: 实体类包信息  
    key: value  
    ……  
  specPackage: spec包信息 
    key: value  
    ……    
  mapperPath: mapper包信息  
    key: value  
    ……  
  mapperInterfacePackage: dao接口包信息  
    key: value  
    ……  
  serviceInterfacePackage: service接口包信息  
    key: value  
    ……  
  serviceImplPackage: service实现包信息    
    key: value  
    ……  
  serviceProviderVersion: service dubbo版本 
    key: value  
    ……  
  controllerPackage: controller包信息  
    key: value  
    ……  
  viewPath: 界面包信息  
    key: value  
    ……  
  jsPath: js包信息  
    key: value  
    ……  

配置六：
template-info: 模板信息（模板文件名为空时不生成模板）  
  basePath: /templates（模板路径）  
  entity: 实体模板文件名  
  entityPath: 实体生成路径（以 '\\' 结尾）  
  spec: spec模板文件名 
  specPath: spec生成路径（以 '\\' 结尾）  
  mapper: mapper模板文件名  
  mapperRootPath: mapper生成路径（以 '\\' 结尾）  
  mapperInterface: dao接口文件名  
  mapperInterfacePath: dao接口生成路径（以 '\\' 结尾）  
  serviceInterface: service接口  
  serviceInterfacePath: service接口生成路径（以 '\\' 结尾）  
  serviceImpl: Service实现文件名  
  serviceImplPath: Service实现生成路径（以 '\\' 结尾）  
  controller: Controller模板文件名  
  controllerPath: Controller生成路径（以 '\\' 结尾）  
  view: 界面模板文件名  
  js: js模板文件名  
  webappPath: webapp根目录（以 '\\' 结尾）  
  viewRootPath: view目录（以 '\\' 结尾）  
  
配置七：  
base-class-info: base类信息  
  baseMapperReference: BaseMapper全类名  
  baseEntityReference: BaseEntity全类名  
  baseServiceReference: IBaseService全类名  
  baseServiceImplReference: BaseServiceImpl全类名  
  providerVersionReference: ProviderVersion全类名


yml配置方式：  
simpleProp: simplePropValue  
 arrayProps: 1,2,3,4,5  #数组  
 listProp1:  #List<Map<String, String>>  
 - name: abc  
   value: abcValue  
 - name: efg  
   value: efgValue  
 listProp2:  #List<Map<String, String>>  
  name: value  
  name: value  
 listProp3:  #List  
 - config2Value1   
 - config2Vavlue2  
 mapProps:  #Map  
  key1: value1  
  key2: value2  
