<%@ include file="/WEB-INF/view/include/baseassets.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${tableInfo.remark!}</title>
</head>
<body>
<%@ include file="/WEB-INF/view/include/menu.jsp"%>

</body>
<script src="${r'${ctxAsset}'}/${jsAssetPath?replace(".", "/")!}/${tableInfo.entityName?uncap_first!}.js"></script>
</html>