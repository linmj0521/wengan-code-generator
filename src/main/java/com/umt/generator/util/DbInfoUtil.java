package com.umt.generator.util;

import com.google.common.collect.Lists;
import com.umt.generator.constants.Constants;
import com.umt.generator.constants.IgnoreColumns;
import com.umt.generator.dao.ColumnInfo;
import com.umt.generator.dao.TableInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 获取数据库基本信息的工具类
 */
@Component
@ConfigurationProperties(prefix = "db")
public class DbInfoUtil {

    private static String DRIVER;

    private static String URL;

    private static String USERNAME;

    private static String PASSWORD;

    private static String DBTYPE;

    private static String DBNAME;

    public void setDriver(String driver) {
        DbInfoUtil.DRIVER = driver;
    }

    public void setUrl(String dbUrl) {
        DbInfoUtil.URL = dbUrl;
    }

    public void setUsername(String username) {
        DbInfoUtil.USERNAME = username;
    }

    public void setPassword(String password) {
        DbInfoUtil.PASSWORD = password;
    }

    public void setDbType(String dbType) {
        DbInfoUtil.DBTYPE = dbType.toLowerCase();
    }

    public void setDbName(String dbName) {
        DbInfoUtil.DBNAME = dbName;
    }

    public static TableInfo getTableInfo(String tableName, String entityName) {
        Connection conn = null;
        ResultSet tableRS = null;
        ResultSet columnRS = null;
        TableInfo tableInfo = null;

        try {
            conn = getConn();
            DatabaseMetaData metaData = conn.getMetaData();

            String[] types = {"TABLE"};
            String catalog = null;
            String schemaPattern = null;
            String tableNamePattern = null;
            String columnNamePattern = null;

            if (Constants.DB_ORACLE.equals(DBTYPE)) {
                schemaPattern = metaData.getUserName();
                columnNamePattern = Constants.PERCENT;
            } else if (Constants.DB_SQLSERVER.equals(DBTYPE)) {
                tableNamePattern = Constants.PERCENT;
                columnNamePattern = Constants.PERCENT;
            } else if (Constants.DB_MYSQL.equals(DBTYPE)) {
                schemaPattern = DBNAME;
            } else {
                throw new  RuntimeException("不支持的数据库类型");
            }
            tableRS = metaData.getTables(catalog, schemaPattern, tableNamePattern, types);

            while (tableRS.next()) {
                if (tableName.equals(tableRS.getString("TABLE_NAME"))) {
                    tableInfo = new TableInfo();
                    tableInfo.setName(tableName);
                    tableInfo.setEntityName(entityName);

                    // 表注释
                    tableInfo.setRemark(tableRS.getString("REMARKS"));

                    // 表类型
                    tableInfo.setType(tableRS.getString("TABLE_TYPE"));

                    // 所有列
                    columnRS = metaData.getColumns(null, schemaPattern, tableName, columnNamePattern);
                    List<ColumnInfo> columns = Lists.newArrayList();
                    while (columnRS.next()) {
                        String columnName = columnRS.getString("COLUMN_NAME");
                        if (!IgnoreColumns.columnNames.contains(columnName.toUpperCase())) {
                            ColumnInfo columnInfo = new ColumnInfo();

                            // 列名
                            columnInfo.setColumnName(columnName);
                            columnInfo.setName(StringHelper.underLine2Hump(columnName));

                            // 注释
                            columnInfo.setRemark(columnRS.getString("REMARKS"));

                            // 类型
                            String dataType = columnRS.getString("TYPE_NAME");
                            // 精度
                            int precision = columnRS.getInt("DECIMAL_DIGITS");
                            // oracle number类型精度为0时处理为Integer或Long
                            if (Constants.DB_ORACLE.equals(DBTYPE) && Constants.DBTYPE_NUMBER.equals(dataType) && 0 == precision) {
                                // 长度
                                int length = columnRS.getInt("COLUMN_SIZE");
                                if (length <= 32) {
                                    columnInfo.setType("Integer");
                                } else {
                                    columnInfo.setType("Long");
                                }
                            } else {
                                String javaType = DataTypeConversion.getJavaType(dataType, DBTYPE);
                                if ("BigDecimal".equals(javaType)) {
                                    tableInfo.setExistBigDecimal(true);
                                } else if ("Date".equals(javaType)) {
                                    tableInfo.setExistDate(true);
                                }
                                columnInfo.setType(javaType);
                            }

                            columns.add(columnInfo);
                        }
                    }
                    tableInfo.setColumns(columns);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConn(conn, null, tableRS);
            closeConn(conn, null, columnRS);
        }
        return tableInfo;
    }

    /**
     * 根据数据库的连接参数，获取指定表的基本信息：字段名、字段类型、字段注释
     * @param tableNames	表名
     * @return Map集合
     */
    public static List<TableInfo> getTableInfo(Map<String, String> tableNames) {
        Connection conn = null;
        ResultSet tableRS = null;
        ResultSet columnRS = null;
        List<TableInfo> tableInfos = null;

        try {
            conn = getConn();
            DatabaseMetaData metaData = conn.getMetaData();

            String[] types = {"TABLE"};
            String catalog = null;
            String schemaPattern = null;
            String tableNamePattern = null;
            String columnNamePattern = null;

            if (Constants.DB_ORACLE.equals(DBTYPE)) {
                schemaPattern = metaData.getUserName();
                columnNamePattern = Constants.PERCENT;
            } else if (Constants.DB_SQLSERVER.equals(DBTYPE)) {
                tableNamePattern = Constants.PERCENT;
                columnNamePattern = Constants.PERCENT;
            } else if (Constants.DB_MYSQL.equals(DBTYPE)) {
                schemaPattern = DBNAME;
            } else {
                throw new RuntimeException("不支持的数据库类型");
            }
            tableRS = metaData.getTables(catalog, schemaPattern, tableNamePattern, types);
            tableInfos = Lists.newArrayList();
            while (tableRS.next()) {
                String tableName = tableRS.getString("TABLE_NAME");
                String entityName = tableNames.get(tableName);
                if (StringUtils.isNoneBlank(entityName)) {
                    TableInfo tableInfo = new TableInfo();
                    tableInfo.setName(tableName);
                    tableInfo.setEntityName(entityName);

                    // 表注释
                    tableInfo.setRemark(tableRS.getString("REMARKS"));

                    // 表类型
                    tableInfo.setType(tableRS.getString("TABLE_TYPE"));

                    // 所有列
                    columnRS = metaData.getColumns(null, schemaPattern, tableName, columnNamePattern);
                    List<ColumnInfo> columns = Lists.newArrayList();
                    while (columnRS.next()) {
                        String columnName = columnRS.getString("COLUMN_NAME");
                        if (!IgnoreColumns.columnNames.contains(columnName.toUpperCase())) {
                            ColumnInfo columnInfo = new ColumnInfo();

                            // 列名
                            columnInfo.setColumnName(columnName);
                            columnInfo.setName(StringHelper.underLine2Hump(columnName));

                            // 注释
                            columnInfo.setRemark(columnRS.getString("REMARKS"));

                            // 类型
                            String dataType = columnRS.getString("TYPE_NAME");
                            // 精度
                            int precision = columnRS.getInt("DECIMAL_DIGITS");
                            // oracle number类型精度为0时处理为Integer或Long
                            if (Constants.DB_ORACLE.equals(DBTYPE) && Constants.DBTYPE_NUMBER.equals(dataType) && 0 == precision) {
                                // 长度
                                int length = columnRS.getInt("COLUMN_SIZE");
                                if (length <= 32) {
                                    columnInfo.setType("Integer");
                                } else {
                                    columnInfo.setType("Long");
                                }
                            } else {
                                String javaType = DataTypeConversion.getJavaType(dataType, DBTYPE);
                                if ("BigDecimal".equals(javaType)) {
                                    tableInfo.setExistBigDecimal(true);
                                } else if ("Date".equals(javaType)) {
                                    tableInfo.setExistDate(true);
                                }
                                columnInfo.setType(javaType);
                            }

                            columns.add(columnInfo);
                        }
                    }
                    tableInfo.setColumns(columns);
                    tableInfos.add(tableInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConn(conn, null, tableRS);
            closeConn(conn, null, columnRS);
        }
        return tableInfos;
    }

    /**
     * 获取数据库连接
     * @return
     */
    private static Connection getConn() {
        Connection conn = null;
        try {
            Properties props = new Properties();
            props.put("remarksReporting", "true");
            props.put("user", USERNAME);
            props.put("password", PASSWORD);
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(URL, props);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 关闭数据库连接
     * @param conn
     * @param stat
     * @param resultSet
     */
    private static void closeConn(Connection conn, Statement stat, ResultSet resultSet) {
        try {
            if (conn != null) conn.close();
            if (stat != null) stat.close();
            if (resultSet != null) resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}