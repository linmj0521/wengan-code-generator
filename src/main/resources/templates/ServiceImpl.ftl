package ${serviceImplPackage!};

import com.alibaba.dubbo.config.annotation.Service;
import ${baseClassInfo.providerVersionReference!};
import ${baseClassInfo.baseServiceImplReference!};
import ${mapperInterfacePackage!}.${tableInfo.entityName!}Mapper;
import ${entityPackage!}.${tableInfo.entityName!};
import ${serviceInterfacePackage}.I${tableInfo.entityName!}Service;
import ${specPackage}.${tableInfo.entityName!}Spec;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Service Impl: ${tableInfo.remark!}
 *
 * @author ${author!}
 * @date ${createDate!}
 */
@Slf4j
@Component("${tableInfo.entityName?uncap_first!}Service")
@Service(version = ${baseClassInfo.providerVersionName!}.${serviceProviderVersion!})
public class ${tableInfo.entityName!}ServiceImpl extends BaseServiceImpl<${tableInfo.entityName!}Mapper, ${tableInfo.entityName!}, ${tableInfo.entityName!}Spec> implements I${tableInfo.entityName!}Service {

}