package com.umt.generator.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public final class BeanHelper implements ApplicationContextAware {
    private static ApplicationContext context;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
    public final static <T> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass);
    }

    public final static Map<String, Object> bean2Map(Object obj) {
        if(null == obj) {
            return null;
        }
        return getBean(ObjectMapper.class).convertValue(obj, Map.class);
    }
}