package com.umt.generator.util;

import com.google.common.collect.Maps;
import com.umt.generator.constants.Constants;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class DataTypeConversion {

    final static Map<String, String> ORACLE_TYPE_MAP = Maps.newHashMap();
    final static Map<String, String> SQLSERVER_TYPE_MAP = Maps.newHashMap();
    final static Map<String, String> MYSQL_TYPE_MAP = Maps.newHashMap();

    static {
        ORACLE_TYPE_MAP.put("CHAR", "String");
        ORACLE_TYPE_MAP.put("VARCHAR2", "String");
        ORACLE_TYPE_MAP.put("NVARCHAR2", "String");
        ORACLE_TYPE_MAP.put("VARCHAR", "String");
        ORACLE_TYPE_MAP.put("CLOB", "String");
        ORACLE_TYPE_MAP.put("TIMESTAMP", "Date");
        ORACLE_TYPE_MAP.put("DATE", "Date");
        ORACLE_TYPE_MAP.put("INTEGER", "Integer");
        ORACLE_TYPE_MAP.put("FLOAT", "BigDecimal");
        ORACLE_TYPE_MAP.put("NUMBER", "BigDecimal");


        MYSQL_TYPE_MAP.put("BIGINT", "Long");
        MYSQL_TYPE_MAP.put("DATETIME", "Date");
        MYSQL_TYPE_MAP.put("DATE", "Date");
        MYSQL_TYPE_MAP.put("VARCHAR", "String");
        MYSQL_TYPE_MAP.put("DECIMAL", "BigDecimal");
        MYSQL_TYPE_MAP.put("INT", "Integer");
        MYSQL_TYPE_MAP.put("CHAR", "String");


        SQLSERVER_TYPE_MAP.put("SMALLINT", "short");
        SQLSERVER_TYPE_MAP.put("INT", "Integer");
        SQLSERVER_TYPE_MAP.put("BIGINT", "Long");
        SQLSERVER_TYPE_MAP.put("REAL", "Float");
        SQLSERVER_TYPE_MAP.put("FLOAT", "Double");
        SQLSERVER_TYPE_MAP.put("DECIMAL", "BigDecimal");
        SQLSERVER_TYPE_MAP.put("NUMERIC", "BigDecimal");
        SQLSERVER_TYPE_MAP.put("BIT", "Boolean");
        SQLSERVER_TYPE_MAP.put("CHAR", "String");
        SQLSERVER_TYPE_MAP.put("VARCHAR", "String");
        SQLSERVER_TYPE_MAP.put("TEXT", "String");
        SQLSERVER_TYPE_MAP.put("NCHAR", "String");
        SQLSERVER_TYPE_MAP.put("NVARCHAR", "String");
        SQLSERVER_TYPE_MAP.put("DATE", "Date");
        SQLSERVER_TYPE_MAP.put("DATETIME2", "Date");
        SQLSERVER_TYPE_MAP.put("TIME", "Date");
        SQLSERVER_TYPE_MAP.put("DATETIME", "Date");
    }

    public static String getJavaType(String dataType, String dbType) {
        String javaType = null;
        if (Constants.DB_ORACLE.equals(dbType)) {
            if (dataType.indexOf(Constants.CHARACTER_LEFT_PARENTHESES) > 0) {
                javaType = ORACLE_TYPE_MAP.get(dataType.toUpperCase().substring(0, dataType.indexOf(Constants.CHARACTER_LEFT_PARENTHESES)));
            } else {
                javaType = ORACLE_TYPE_MAP.get(dataType.toUpperCase());
            }
        } else if (Constants.DB_SQLSERVER.equals(dbType)) {
            javaType =  SQLSERVER_TYPE_MAP.get(dataType.toUpperCase());
        } else if (Constants.DB_MYSQL.equals(dbType)) {
            javaType =  MYSQL_TYPE_MAP.get(dataType.toUpperCase());
        }
        return StringUtils.isBlank(javaType) ? StringUtils.capitalize(dataType.toLowerCase()) : javaType;
    }
}
