<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${mapperInterfacePackage!}.${tableInfo.entityName!}Mapper">

    <!-- 新增 @author ${author!} on ${createDate!} -->
    <insert id="insert">
        INSERT INTO ${tableInfo.name!} (
          ID,
          CREATE_BY,
          CREATE_DATE,
          DEL_FLAG,
          <#if tableInfo.columns?exists>
              <#list tableInfo.columns as column>
          ${column.columnName!}<#if column_has_next>,</#if>
              </#list>
          </#if>
        )
        VALUES (
          ${r'#{id}'},
          ${r'#{createBy.id}'},
          ${r'#{createDate}'},
          '0',
          <#if tableInfo.columns?exists>
              <#list tableInfo.columns as column>
          ${r'#{'}${column.name!}}<#if column_has_next>,</#if>
              </#list>
          </#if>
        )
    </insert>

    <!-- 修改 @author ${author!} on ${createDate!} -->
    <update id="update">
        UPDATE ${tableInfo.name!} SET
          UPDATE_BY = ${r'#{'}updateBy.id},
          UPDATE_DATE = ${r'#{'}updateDate},
          <#if tableInfo.columns?exists>
              <#list tableInfo.columns as column>
          ${column.columnName!} = ${r'#{'}${column.name!}}<#if column_has_next>,</#if>
              </#list>
          </#if>
        WHERE
          ID = ${r'#{id}'}
    </update>

    <!-- 删除 @author ${author!} on ${createDate!} -->
    <update id="delete">
        UPDATE ${tableInfo.name!} SET
          DEL_FLAG = '1',
          UPDATE_BY = ${r'#{'}updateBy.id},
          UPDATE_DATE = ${r'#{'}updateDate}
        WHERE ID = ${r'#{id}'}
    </update>

    <!-- resultMap @author ${author!} on ${createDate!} -->
    <resultMap id="${tableInfo.entityName?uncap_first!}ResultMap" type="${tableInfo.entityName!}">
        <result property="id" column="a.id"/>
        <result property="createDate" column="a.createDate"/>
        <result property="updateDate" column="a.updateDate"/>
        <result property="delFlag" column="a.delFlag"/>
        <#if tableInfo.columns?exists>
            <#list tableInfo.columns as column>
        <result property="${column.name!}" column="a.${column.name!}"/>
            </#list>
        </#if>
    </resultMap>

    <!-- 基础字段sql @author ${author!} on ${createDate!} -->
    <sql id="baseSql">
        a.ID AS "a.id",
        a.CREATE_DATE AS "a.createDate",
        a.UPDATE_DATE AS "a.updateDate",
        a.DEL_FLAG AS "a.delFlag",
        <#if tableInfo.columns?exists>
            <#list tableInfo.columns as column>
        a.${column.columnName!} AS "a.${column.name!}"<#if column_has_next>,</#if>
            </#list>
        </#if>
    </sql>

    <!-- 根据id查询 @author ${author!} on ${createDate!} -->
    <select id="findById" resultMap="${tableInfo.entityName?uncap_first!}ResultMap">
        SELECT <include refid="baseSql"/> FROM ${tableInfo.name!} WHERE ID = ${r'#{id}'}
    </select>

    <!-- 分页条件查询 @author ${author!} on ${createDate!} -->
    <!-- eg: SELECT <include refid="baseSql"/> FROM ${tableInfo.name!} WHERE ID = ${r'#{id}'}-->
    <select id="findBySpecForPage" resultMap="${tableInfo.entityName?uncap_first!}ResultMap">
        SELECT <include refid="baseSql"/> FROM ${tableInfo.name!}
    </select>

</mapper>