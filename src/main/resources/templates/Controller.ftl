package ${controllerPackage!};

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ${baseClassInfo.providerVersionReference!};
import com.alibaba.dubbo.config.annotation.Reference;

import ${serviceInterfacePackage!}.I${tableInfo.entityName!}Service;

/**
 * Controller: ${tableInfo.remark!}
 *
 * @author ${author!}
 * @date ${createDate!}
 */
@Slf4j
@Controller
@RequestMapping("${r'${path.secure}'}/${tableInfo.entityName?uncap_first!}")
public class ${tableInfo.entityName!}Controller {

    @Reference(registry = "${controllerRegistry!}", version = ${baseClassInfo.providerVersionName!}.${serviceProviderVersion!})
    I${tableInfo.entityName!}Service ${tableInfo.entityName?uncap_first!}Service;

    @RequestMapping({"", "/", "/index"})
    public String index() {
        return "/${tableInfo.entityName?uncap_first!}/${tableInfo.entityName?uncap_first!}";
    }

}
